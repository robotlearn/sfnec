# Successor Feature Neural Episodic Control

Source code and experimental evaluation for the paper: ["Successor Feature Neural Episodic Control"](https://arxiv.org/abs/2111.03110v1).

Authors: [David Emukpere](https://www.linkedin.com/in/david-emukpere/), [Xavier Alameda-Pineda](http://xavirema.eu/), [Chris Reinke](https://www.scirei.net/)

Copyright: INRIA, 2021

License: GNU General Public License v3.0 or later

## Setup

Requirements:
 - Python 3.6 or higher.
 - Linux (developed under Ubuntu 18.04) or MacOS.
 - Several python package are required which will be automatically installed during the setup.

To run the experiments the sfnec python package must be installed.
It is recommended to install the package in developer mode (*-e*), so that changes to the source code can be directly used without the need to reinstall the package:

`pip install -e .`

To be able to load the results with our DataLoader widget in Jupyter notebook, run the following command.
(Note: The GUI is currently only tested for Jupyter notebooks. For Jupyterlab, other installation procedures are necessary.)

`jupyter nbextension enable --py --sys-prefix qgrid`

Optional:
We recommend to install the Jupyter notebook extensions which allows to fold the code and sections.

`pip install jupyter_contrib_nbextensions` \
`jupyter contrib nbextension install --user`


## Overview

The source code has 3 components:

 - exputils package: The exputils package provides functions to generate and run the code for the experiments, and to visualize the results.

 - rlexp package: The rlexp package contains the code of the agents, environments and the experimental procedure.

 - experiments folder: Contains the parameter settings and the start scripts to run the experiments.
    The experimental results will be stored here and notebooks exist to visualize them.
    Please see the "Usage" section for information about how to run and visualize the experiments.

    Experiments:
      - object_collection: Object collection environment from Barreto et al. (2018)

## Usage

### Running the Experiments

Each experiment has several parameters.
Each parameter defines which algorithm to use and its hyperparameters.
Several repetitions (for the paper n=10) for each parameter are executed which have a different random seed.  
The exputils package is used to generate for each parameter and repetition the code from a ODS file and code templates.

Each experiment folder contains an *experiment_configurations.ods*.
This file contains the different parameters, one per row.
Each parameter has an ID (called *Experiment-ID*) which must be unique.
Please note, rows in the *experiment_configurations.ods* that have an empty *Experiment-ID* column are ignored.
In the provided ODS files, only the parameters for each algorithm that resulted in the best performance are provided.
Running all parameters used during tuning is highly time-consuming!
If you want to run other parameters, add them in the ODS file.

The exputils package uses code templates under the *src* directory to create for each parameter and repetition (number of repetitions are defined in the *repetitions* column of the ODS) the required code.
These are stored in an *experiments* directory which will be automatically generated.

To generate and run the experiments, run the *run_experiments.sh* script:

`./run_experiments.sh`

The command takes as input argument how many experiments (or better repetitions) should run in parallel.
This saves time if you have several CPU's or cores.
Example: `./run_experiments.sh 6`

Please note, you can add new parameters or a higher number of repetitions after your first run in the *experiment_configurations.ods* file.
If you then run the *run_experiments.sh* script, it will only run the repetitions for the new parameters or where more repetitions are required.

### Analyzing the Results

After the experiments have been finished, the Jupyter notebooks in the *analyze* directory can be used to look at the results.
Each repetition has saved its result in a *data* directory under the *experiments* directory.
The notebooks will load this data and visualize it.

To use the notebooks, first run Jupyter notebook using the following command:

`jupyter notebook`

The *overview.ipynb* notebook is used to visualize the data.
The table-widget that is displayed after running the its first cell, allows to load data from specific parameters.
It also allows to set names for each parameter which are then used in the figures.
Please note, loading data from too many parameters might result in an out-of-memory situation!

The other cells can be used to visualize different aspects from the experiments.
