import rlexp
from repetition_config import config

# set random seeds
rlexp.utils.seed(config)

# run experiment
agent, log = rlexp.exp.continual.run_experiment(config=config)
log.config.numpy_log_mode = 'npz'
log.save()
