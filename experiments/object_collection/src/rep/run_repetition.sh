#!/bin/bash

STATUSFILE=run_repetition.sh.status

CURRENT_DIR=$(pwd)
REP=${CURRENT_DIR##*/}
EXP=${CURRENT_DIR%/*}
EXP=${EXP##*/}

echo "Run ${REP} of ${EXP} ..."
STATE='Running'

date "+%Y/%m/%d %H:%M:%S" >> $STATUSFILE
echo $STATE >>  $STATUSFILE

python run_repetition.py
RETURN_CODE=$?

echo "Write status file ..."
if [ $RETURN_CODE == 0 ] 
then
	STATE='Finished'
else
	STATE='Error'
fi

date "+%Y/%m/%d %H:%M:%S" >> $STATUSFILE
echo $STATE >> $STATUSFILE

echo "Finished."
