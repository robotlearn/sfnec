import torch
import numpy as np
import exputils as eu
import rlexp.agent as agents
import rlexp.env as env

config = eu.AttrDict(
    seed = 1984 + <repetition_id>,

    agent_class = agents.<agent>,

    env_class = env.Barreto2018TwoDObjectCollection,
    env_config = eu.AttrDict(),

    n_tasks = <n_tasks>,
    n_max_steps = <n_max_steps>,
    n_episodes_per_phase = np.inf,
    n_max_steps_per_epsiode = np.inf,
)

# agent configuration depends on the used agent (config.agent_class)

# set general agent config
agent_config = eu.combine_dicts(dict(<agent_config>), config.agent_class.default_config())
agent_config.gamma = <gamma>


if config.agent_class == agents.Barreto2018SFQL:
    agent_config.epsilon = <epsilon>
    agent_config.alpha = <lr>
    agent_config.alpha_w = <alpha_w>

if config.agent_class == agents.NECAgent or config.agent_class == agents.SFNECAgent:
    agent_config.key_size = <key_size>
    agent_config.dnd_capacity = <dnd_capacity>
    agent_config.dnd_alpha = <dnd_alpha>
    agent_config.num_neighbours = <num_neighbours>
    agent_config.n_step_horizon = <n_step_horizon>
    agent_config.nec = eu.combine_dicts(dict(<nec_network_config>), agent_config.nec)
    agent_config.nec.optimizer.lr = <lr>

if config.agent_class == agents.SFNECAgent:
    agent_config.alpha_w = <alpha_w>

config.agent_config = agent_config

##################
# sample random tasks
env = config.env_class(config=config.env_config)
feature_dim = env.config.object_features.shape[1]

# set seed so that the sampling of the tasks is deterministic
eu.misc.seed(config)
# sample weights between [-1, 1]
reward_weights_per_task = np.random.rand(config.n_tasks, feature_dim) * 2 - 1
# add weight of 1 for the feature of ending in the goal state
reward_weights_per_task = np.hstack((reward_weights_per_task, np.ones((config.n_tasks, 1))))

reward_functions = []
for task_idx in range(config.n_tasks):
    reward_weights = reward_weights_per_task[task_idx, :]
    reward_func = lambda feature, w=reward_weights: np.sum(w * np.array(feature))
    reward_func_descr = dict(reward_weight_vector=reward_weights)
    reward_functions.append((reward_func, reward_func_descr))

config.reward_functions = reward_functions

config.reward_weights = reward_weights_per_task

