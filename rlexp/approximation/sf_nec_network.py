import numpy as np
import torch
from .dnd_sf import DND_SF

class SFNEC(torch.nn.Module):
    """
    Implementation of succesor neural episodic control architecture as introduced in 
    Successor Feature Neural Episodic Control
    """

    def __init__(self, embedding_net, n_actions, config=None):
        super().__init__()
        self.embedding_net = embedding_net
        self.dnds = torch.nn.ModuleList([DND_SF(config) for _ in range(n_actions)])

    def forward(self, observations):
        """
        Batch mode forward pass through entire NEC network
        """
        keys = self.embedding_net(observations)
        psis = torch.stack([dnd(keys) for dnd in self.dnds])
        # to get psi_values in shape [batch_size x actions x SF_shape] SF_shape := value_size
        psis = torch.transpose(psis, 0, 1)
        return psis

    def lookup(self, obs):
        """
        Single mode forward pass through entire NEC network without gradient tracking
        """
        with torch.no_grad():
            key = self.embedding_net(obs.unsqueeze(0))
            psis = np.array([dnd(key, training=True).detach().cpu().numpy() for dnd in self.dnds])

            return psis, key.squeeze(0) # remove batch dimension

    def update_memory(self, action, keys, values):
        """
        Used to batch update an action's DND
        """
        self.dnds[action].update_batch(keys, values)